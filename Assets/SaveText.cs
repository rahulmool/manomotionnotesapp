﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TMPro.Examples {
    public class SaveText : MonoBehaviour
    {

        [SerializeField]
        private TextMeshPro textField;

        public void Finallysavetext(string data)
        {
            textField.SetText(data, true);
        }

    }

}

