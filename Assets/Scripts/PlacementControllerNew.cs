﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class PlacementControllerNew : MonoBehaviour
{
    [SerializeField]
    public GameObject placedPrefab;
    private GameObject spawnedPrefab;
    private ARRaycastManager arRaycastmanager;
    private Vector2 touchPosition;
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();
    private bool setnotetext = false,setnotecolor=false;
    private string notetext,m;
    private int notecount = 0, gameobjectcount = 0;


    private void Start()
    {
        //Setnotetext("<size=100%><b>Check new</b></b></size><br><br><size=80%>Check new</size>");
    }

    private void Setnotetext(string data)
    {
        FlutterUnityPlugin.Message message = FlutterUnityPlugin.Messages.Receive(data);
        //string value = message.data;
        //placedPrefab.GetComponent<ARCubeInteraction>().Set3dnotetext(value);
        notetext = message.data;
        message.data = "text saved as " + message.data + "successfully executed";
        FlutterUnityPlugin.Messages.Send(message);
        setnotetext = true;
    }

    private void Setnotecolor(string data)
    {
        FlutterUnityPlugin.Message message = FlutterUnityPlugin.Messages.Receive(data);
        string value = message.data;
        m = message.data;
        //placedPrefab.GetComponent<ARCubeInteraction>().setm(value);
        message.data = "text saved as " + value + "successfully executed";
        FlutterUnityPlugin.Messages.Send(message);
        setnotecolor = true;
        notecount++;
    }

    private void Awake()
    {
        arRaycastmanager = GetComponent<ARRaycastManager>();
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            touchPosition = Input.GetTouch(index: 0).position;
            return true;
        }
        touchPosition = default;
        return false;
    }

    private void Update()
    {
        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;
        if (arRaycastmanager.Raycast(touchPosition, hits, trackableTypes: TrackableType.PlaneWithinPolygon)&& setnotecolor && setnotetext&& notecount>gameobjectcount)
        {
            var hitPose = hits[0].pose;
            spawnedPrefab = Instantiate(placedPrefab, hitPose.position, hitPose.rotation);
            var x = spawnedPrefab.transform.rotation;
            spawnedPrefab.transform.rotation = Quaternion.Euler(x.x, x.y, 0);
            spawnedPrefab.GetComponent<ARCubeInteraction>().Set3dnotetext(notetext);
            spawnedPrefab.GetComponent<ARCubeInteraction>().setm(m);
            setnotecolor = false;
            setnotetext = false;
            gameobjectcount++;
        }
    }
}
